#!/usr/bin/env python

import ptvsd
import time
import os
import threading
from gui import mainWindow
from domoticz import domoticz
import requests
from bs4 import BeautifulSoup

print ("Application started\n")
# Allow other computers to attach to ptvsd at this IP address and port.
ptvsd.enable_attach(address=('192.168.1.150', 3000), redirect_output=True)

print ("Waiting for attach\n")

# Pause the program until a remote debugger is attached
ptvsd.wait_for_attach()

print ("Attach done\n")

class DomoLythCtrl():

    num_room = 11
    active_room = 0
    room_name = ["Salon", "Bureau", "Bibliotheque", "Cellier", "Sdb Bas", "Sdb Haut", "Chambre", "Lingerie", "Atelier", "Labo", "Nurserie"]
    hasThermostat = [True, True,     True,           True,      True,      True,       True,      True,       True,      True,   True,   ]
    allOff = False

    #Heater Mode label.
    HeaterModeLabel = ["OFF", "H-G", "ECO", "CONF"]

    SETWIN_NONE = 0
    SETWIN_CONF = 1
    SETWIN_ECO = 2
    set_window = SETWIN_NONE
    consigne = 19.5
    meteo = (False, "Pas de pluie")
    meteo_timer_reload = (15 * 60)
    meteo_timer = 0
    

    def __init__(self):
        self.gui = mainWindow.mw()
        self.dom = domoticz.domoticz("192.168.1.133:8080")
        threading.Thread(target = self.statusPooling).start()
        self.guilock = threading.Lock()
        self.gui._display.setRoom (self.room_name[self.active_room])
        self.gui._rotary.registerStepCb (self.rotaryStep)
        self.gui._pushOk.when_pressed = self.okPressed
        self.gui._pushCancel.when_pressed = self.cancelPressed
        self.gui._pushMode.when_pressed = self.modePressed
        self.gui._pushOnOff.when_pressed = self.onOffPressed
        
       
    def mainloop (self):
        while True:
            time.sleep(1)

    def statusPooling(self):
        while (1):
            print ("Status update.")
            self.updateStatus()
            if (self.devices != None):
                self.updateDisplay()
            else:
                self.updateDisplayNoConnection()
            self.meteo_timer = self.meteo_timer - 10
            if (self.meteo_timer <= 0):
                self.updateMeteo()
                self.updateMeteoDisplay()
                self.meteo_timer = self.meteo_timer_reload
            time.sleep(10)

    def updateStatus(self):
        self.devices = self.dom.list()

    def updateMeteo(self):
        url = "https://fr.meteox.com/fr-fr/forecastlocation/t/1817325/saint-arnoult-en-yvelines"
        try:
            r=requests.get(url)
        except requests.exceptions.RequestException as e:  # This is the correct syntax
            r=None

        if ((r != None) and (r.status_code == 200)):
            meteox = BeautifulSoup(r.text, 'html.parser')
            rain = (False, "Pas de pluie") 
            for link in meteox.find_all('div', class_="forecast-today"):
                precipitation = link.find('div', class_="precipitation")
                hour = ''.join(e for e in link.contents[2] if not e.isspace()).split(":")
                hour[0] = int(hour[0]) + 1
                if (hour[0] > 23):
                    hour[0] = 0
                
                mm = float(precipitation.contents[3].contents[0].split()[0].replace(",","."))
                if (mm > 0):
                    rain = (True, str ("%dh%s: %s" % (hour[0], hour[1], precipitation.contents[3].contents[0])))
                    break
        else:
            rain = (False, "Indisponible") 

        self.meteo = rain
        self.updateMeteoDisplay()

    def updateDisplayNoConnection(self):
        self.guilock.acquire()
        self.gui._display.setTempIn(-99)
        self.gui._display.setTempOut(-99)
        self.gui._display.setHumidity(-1)
        self.guilock.release ()

    def updateDisplay(self):
        if not self.set_window:
            
            temp_name = "Temp " + self.room_name[self.active_room]
            heater_name = "Radiateur " + self.room_name[self.active_room]
            therm_mode_name = self.room_name[self.active_room] + " - Thermostat Mode"
            therm_ctrl_name = self.room_name[self.active_room] + " - Thermostat Control"
            setPt_confort_name = self.room_name[self.active_room] + " - Confort"
            setPt_eco_name = self.room_name[self.active_room] + " - Eco"

            ext_temp = -99
            ext_hum = -1
            temp = -99
            heater_mode = -1
            therm_mode = -1
            therm_ctrl = -1
            setPt_confort = -99
            setPt_eco = -99

            for device in self.devices['result']:
                if (device['Name'] == "Temp Ext"):
                    ext_temp = device['Temp']
                    ext_hum = device['Humidity']
                elif (device['Name'] == temp_name):
                    temp = device['Temp']
                elif (device['Name'] == heater_name):
                    heater_mode = int(device ['Level'] / 10)     
                elif (device['Name'] == therm_mode_name):
                    therm_mode = int(device ['Level'] / 10)
                elif (device['Name'] == therm_ctrl_name):
                    therm_ctrl = int(device ['Level'] / 10)
                elif (device['Name'] == setPt_confort_name):
                    setPt_confort = device['SetPoint']
                elif (device['Name'] == setPt_eco_name):
                    setPt_eco = device['SetPoint']
            self.guilock.acquire()
            self.gui._display.setRoom (self.room_name[self.active_room])
            self.gui._display.setTempIn(temp)
            self.gui._display.setTempOut(ext_temp)
            self.gui._display.setHumidity(ext_hum)
            if (self.hasThermostat[self.active_room] == False): #No thermostat for room.
                self.gui._display.setMode (self.HeaterModeLabel[heater_mode])
                self.gui._display.clearConsigne ()
            else:
                if (therm_ctrl == 0):
                    self.gui._display.setMode ("OFF")
                    self.gui._display.clearConsigne ()
                elif (therm_ctrl == 2):
                    self.gui._display.setMode ("FORCE")
                    self.gui._display.clearConsigne ()
                else:
                    if (therm_mode == 1):
                        self.gui._display.setMode ("CONF")
                        self.gui._display.setConsigne (float(setPt_confort))
                    else:
                        self.gui._display.setMode ("ECO")
                        self.gui._display.setConsigne (float(setPt_eco))        
            self.guilock.release ()
   
    def updateMeteoDisplay(self):
        if (self.set_window == self.SETWIN_NONE):
            self.guilock.acquire ()
            self.gui._display.setMeteo (self.meteo[0], self.meteo[1])
            self.guilock.release ()


    def rotaryStep(self, dir):
        if (self.set_window == self.SETWIN_NONE):
            if (dir == 0):
                self.active_room = self.active_room - 1
                if (self.active_room < 0):
                    self.active_room = self.num_room - 1
            else:
                self.active_room = self.active_room + 1
                if (self.active_room >=  self.num_room):
                    self.active_room = 0
            self.updateDisplay()
        else:
            if (dir == 0):
                if (self.consigne > 12):
                    self.consigne = self.consigne - 0.5
            else:
                if (self.consigne < 26):
                    self.consigne = self.consigne + 0.5
            self.guilock.acquire()
            self.gui._display.setSetTemp(self.consigne)
            self.guilock.release ()
            self.set_window_timer.cancel()
            self.set_window_timer = threading.Timer (10, self.setWindowTimeout)
            self.set_window_timer.start()


    def okPressed (self):
        if (self.set_window == self.SETWIN_NONE):
            #check that the room has a thermostat.
            if (self.hasThermostat[self.active_room] == True):
                self.set_window = self.SETWIN_CONF
                setPt_name = self.room_name[self.active_room] + " - Confort"
                for device in self.devices['result']:
                    if (device['Name'] == setPt_name):
                        self.consigne = float(device ['SetPoint'])
                self.guilock.acquire ()
                self.gui._display.drawSetDispBackground()
                self.gui._display.setSetRoom(self.room_name[self.active_room])
                self.gui._display.setSetMode ("CONF")
                self.gui._display.setSetTemp(self.consigne)
                self.guilock.release ()
                self.set_window_timer = threading.Timer (10, self.setWindowTimeout)
                self.set_window_timer.start()
        elif (self.set_window == self.SETWIN_CONF):
            self.set_window = self.SETWIN_ECO
            setPt_conf_name = self.room_name[self.active_room] + " - Confort"
            setPt_name = self.room_name[self.active_room] + " - Eco"
            for device in self.devices['result']:
                if (device['Name'] == setPt_name):
                    self.consigne = float(device ['SetPoint'])
                elif (device['Name'] == setPt_conf_name):
                    self.dom.set_set_point(device['idx'], str(self.consigne))
            self.guilock.acquire ()
            self.gui._display.setSetMode ("ECO")                                
            self.gui._display.setSetTemp(self.consigne)
            self.guilock.release ()
            self.set_window_timer.cancel()
            self.set_window_timer = threading.Timer (10, self.setWindowTimeout)
            self.set_window_timer.start()
        else:
            self.set_window_timer.cancel()
            setPt_name = self.room_name[self.active_room] + " - Eco"
            for device in self.devices['result']:
                if (device['Name'] == setPt_name):
                    self.dom.set_set_point(device['idx'], str(self.consigne))
            self.guilock.acquire ()
            self.gui._display.drawMainDispBackground()
            self.guilock.release ()
            self.set_window = self.SETWIN_NONE
            self.updateStatus()
            self.updateDisplay ()
            self.updateMeteoDisplay ()
    
    def cancelPressed (self):
        if (self.set_window != self.SETWIN_NONE):
            self.set_window_timer.cancel()
            self.guilock.acquire ()
            self.gui._display.drawMainDispBackground()
            self.guilock.release ()
            self.set_window = False
            self.updateDisplay ()
            self.updateMeteoDisplay ()
        elif (self.hasThermostat[self.active_room] == True):
            therm_mode_name = self.room_name[self.active_room] + " - Thermostat Mode"
            for device in self.devices['result']:   
                if (device['Name'] == therm_mode_name):
                    therm_mode = int(device['Level'])
                    if (therm_mode == 10):
                        self.dom.dim(device['idx'],20)
                    else:
                        self.dom.dim(device['idx'],10)
            self.updateStatus()
            self.updateDisplay()
        elif (self.hasThermostat[self.active_room] == False):
            heater_name = "Radiateur " + self.room_name[self.active_room]
            for device in self.devices['result']:   
                if (device['Name'] == heater_name):
                    mode = int(device ['Level'])
                    if (mode == 30):
                        self.dom.dim(device['idx'],20)
                    else:
                        self.dom.dim(device['idx'],30)
            self.updateStatus()
            self.updateDisplay()


    def modePressed (self):
        if self.set_window == self.SETWIN_NONE:
            if (self.hasThermostat[self.active_room] == False):
                heater_name = "Radiateur " + self.room_name[self.active_room]
                for device in self.devices['result']:   
                    if (device['Name'] == heater_name):
                        mode = int(device ['Level'])
                        mode = mode + 10
                        if mode > 30:
                            mode = 0
                        self.dom.dim(device['idx'],mode)
                        self.updateStatus()
                        self.updateDisplay()
            else:
                therm_ctrl_name = self.room_name[self.active_room] + " - Thermostat Control"
                for device in self.devices['result']:   
                    if (device['Name'] == therm_ctrl_name):
                        therm_ctrl = int(device['Level'])
                        if (therm_ctrl == 0):
                            self.dom.dim(device['idx'],10)
                        elif (therm_ctrl == 10):
                            self.dom.dim(device['idx'],20)
                        else: 
                            self.dom.dim(device['idx'],0) 
                self.updateStatus()
                self.updateDisplay()

    def onOffPressed (self):
        therm_ctrl = " - Thermostat Control"
        for device in self.devices['result']:
            if (device['Name'].find (therm_ctrl) != -1):
                if (self.allOff):
                    self.dom.dim(device['idx'],10)
                else:
                    self.dom.dim(device['idx'],0)
        if (self.allOff):
            self.allOff = False
        else:
            self.allOff = True
        self.updateStatus()
        self.updateDisplay()

    def setWindowTimeout (self):
        if (self.set_window):
            self.guilock.acquire ()
            self.gui._display.drawMainDispBackground()
            self.guilock.release ()
            self.set_window = self.SETWIN_NONE
            self.updateDisplay ()
            self.updateMeteoDisplay ()


ctrl = DomoLythCtrl()
ctrl.mainloop()




