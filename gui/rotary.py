from gpiozero import Button



class rotary():
    def __init__(self):
        self._rotA = Button(pin=2, pull_up=True)
        self._rotB = Button(pin=3, pull_up=True)

        self._rotA.when_pressed = self.rotaryTurned
        self._stepCallbacks = []

    def rotaryTurned (self):
        if self._rotB.is_active:
            self.fireStepCb(0)
            print ("Rotary turned right")
        else:
            self.fireStepCb(1)
            print ("Rotary turned left")
   
    def registerStepCb (self, callback):
        self._stepCallbacks.append(callback)
        
    def fireStepCb (self, dir):
        for fn in self._stepCallbacks:
            fn(dir)
    