from gpiozero import SPIDevice, OutputDevice
import time


DOG_CMD        = 12    # GPIO 12 / pin 32
DOG_RES        = 25    # GPIO 25 / pin 22

DOGXL_WIDTH    = 160
DOGXL_HEIGHT   = 104
DOGXL_PAGE_HEIGHT = 4
DOGXL_NUM_PAGES = int(DOGXL_HEIGHT / DOGXL_PAGE_HEIGHT)

DOGXL_PIXEL_WHITE = 0
DOGXL_PIXEL_LIGHT_GRAY = 1
DOGXL_PIXEL_DARK_GRAY = 2
DOGXL_PIXEL_BLACK = 3

DOGXL_INITIAL_CONTRAST = 40

#DOGXL commands
DOGXL_SYSTEM_RESET      = 0xE2
DOGXL_NOP               = 0xE3                          # no operation
DOGXL_SET_TEMP_COMP     = 0x24                          # set temperature compensation, default -0.05%/°C
DOGXL_SET_PANEL_LOADING = 0x2B                          # set panel loading, default 16~21 nF
DOGXL_SET_PUMP_LOADING  = 0x2F                          # default internal Vlcd (8x pump)
DOGXL_SET_LCD_BIAS_RATIO = 0xEB                         # default 11
DOGXL_SET_VBIAS_POT     = 0x81                          # 1 byte (0~255) to follow setting the contrast, default 0x81
DOGXL_SET_LINE_RATE     = 0xA0                          # default 12,1 Klps
DOGXL_SET_DISPLAY_ENABLE = 0xAE                         # + 1 mask / 0 : exit sleep mode / entering sleep mode
DOGXL_SET_LCD_GRAY_SHADE = 0xD0                         # set the number of used com electrodes (lines number -1)
DOGXL_SET_COM_END       = 0xF1                          # set the number of used com electrodes (lines number -1)

#DOGXL ram address control
DOGXL_SET_AC            = 0x88                          # set ram addres control
DOGXL_AC_WA_MASK        = 1                             # automatic column/page increment wrap arroud (1 : cycle increment)
DOGXL_AC_AIO_MASK       = (1 << 1)                      # auto increment order (0/1 : column/page increment first)
DOGXL_AC_PID_MASK       = (1 << 2)                      # page addres auto increment order (0/1 : +1/-1)

#DOGXL cursor address control
DOGXL_SET_CA_LSB        = 0x00                          # + 4 LSB bits
DOGXL_SET_CA_MSB        = 0x10                          # + 4 MSB bits # MSB + LSB values range : 0~159
DOGXL_SET_PA            = 0x60                          # + 5 bits # values range : 0~26

#DOGXL Display control commands
DOGXL_SET_FIXED_LINES   = 0x90                          # + 4 bits = 2xFL
DOGXL_SET_SCROLL_LINE_LSB   = 0x40                      # + 4 LSB bits scroll up display by N (7 bits) lines
DOGXL_SET_SCROLL_LINE_MSB   = 0x50                      # + 3 MSB bits
DOGXL_SET_ALL_PIXEL_ON  = 0xA4		                    # + 1 mask / 0 : set all pixel on, reverse
DOGXL_SET_INVERSE_DISPLAY   = 0xA6		                # + 1 mask / 0 : inverse all data stored in ram, reverse
DOGXL_SET_MAPPING_CONTROL   = 0xC6		                # control mirorring
DOGXL_SET_MAPPING_CONTROL_LC_MASK = 1		            #
DOGXL_SET_MAPPING_CONTROL_MX_MASK = (1 << 1)	        #
DOGXL_SET_MAPPING_CONTROL_MY_MASK = (1 << 2)	        #

# DOGXL window program mode
DOGXL_SET_WINDOW_PROGRAM_ENABLE	= 0xF8		            # + 1 mask / 0 : enable / disable window programming mode,
                                                        # reset before changing boundaries
DOGXL_SET_WP_STARTING_CA        = 0xF4		            # 1 byte to follow for column number
DOGXL_SET_WP_ENDING_CA          = 0xF6		            # 1 byte to follow for column number
DOGXL_SET_WP_STARTING_PA        = 0xF5		            # 1 byte to follow for page number
DOGXL_SET_WP_ENDING_PA          = 0xF7		            # 1 byte to follow for page number


class refreshArea():
    def __init__(self):
        self.pStart = DOGXL_NUM_PAGES
        self.pEnd = -1
        self.cStart = DOGXL_WIDTH
        self.cEnd = -1
        self.area = [0]

class DogXLRam():
    "Class to handle DOG XL Graphic RAM"
    

    def __init__(self):
        self.ram = [0]*DOGXL_WIDTH * DOGXL_HEIGHT
        self.c_refresh_start = 0 #DOGXL_WIDTH
        self.c_refresh_end = DOGXL_WIDTH-1
        self.p_refresh_start = 0 #DOGXL_NUM_PAGES
        self.p_refresh_end = DOGXL_NUM_PAGES-1
        self.needRefresh = False

    def drawPixel (self, x, y, color=DOGXL_PIXEL_BLACK):

        if ((x < 0) or (x >= DOGXL_WIDTH)):
            return
        if ((y < 0) or (y >= DOGXL_HEIGHT)):
            return

        #page = y >> 2
        #pixelIdx = (y & 0x3) << 1
        #val = self.ram [x + (DOGXL_WIDTH * page)]
        #val = val & ~(0x3 << pixelIdx) & 0xFF
        #val = val | (color << pixelIdx)
        self.ram [x + (DOGXL_WIDTH * y)] = color
        # if (self.c_refresh_start > x):
        #     self.c_refresh_start = x
        # if (self.c_refresh_end < x):
        #     self.c_refresh_end = x
        # if (self.p_refresh_start > page): 
        #     self.p_refresh_start = page
        # if (self.p_refresh_end < page):
        #     self.p_refresh_end = page
        
        #self.needRefresh = True
    
    def clearScreen (self):
        self.ram = [0]*DOGXL_WIDTH * DOGXL_HEIGHT
        self.c_refresh_start = 0
        self.c_refresh_end = DOGXL_WIDTH-1
        self.p_refresh_start = 0
        self.p_refresh_end = DOGXL_NUM_PAGES-1
        self.needRefresh = True

    def checkBondaries (self, x, y, width, height):
        if (x >= DOGXL_WIDTH):
            return 0, 0, 0, 0
        if (y >= DOGXL_HEIGHT):
            return 0, 0, 0, 0
        if (x < 0):
            if ((x + width) < 0):
                return 0, 0, 0, 0
            else:  
                width = width + x
                x = 0
        if (y < 0):
            if ((y + height) < 0):
                return 0, 0, 0, 0
            else:  
                height = height + y
                y = 0
        if ((x + width) >= DOGXL_WIDTH):
            if (x >= DOGXL_WIDTH):
                return 0, 0, 0, 0
            else:
                width = DOGXL_WIDTH -x -1
        if ((y + height) >= DOGXL_HEIGHT):
            if (y >= DOGXL_HEIGHT):
                return 0, 0, 0, 0
            else:
                height = DOGXL_HEIGHT -y -1
        
        return x, y, width, height

        
    def drawRectangle (self, xstart, ystart, width, height, thickness=1, fill=False, color=DOGXL_PIXEL_BLACK):
        xstart, ystart, width, height = self.checkBondaries(xstart,ystart,width,height)
        if (width == 0) or (height == 0):
            return 

        if (fill == True):
            for y in range (height):
                for x in range (width):
                    self.drawPixel(x+xstart, y+ystart, color)
        else:
            startY1 = ystart
            stopY1 = ystart+thickness
            startY2 = ystart+height-thickness
            stopY2 = ystart+height
            startX1 = xstart
            stopX1 = xstart+thickness
            startX2 = xstart+width-thickness
            stopX2 = xstart+width
            for y in range (startY1,stopY1):
                for x in range (width):
                    self.drawPixel(x+xstart, y, color)
            for y in range (stopY1,startY2):
                for x in range (startX1,stopX1):
                    self.drawPixel(x, y, color)
                for x in range (startX2,stopX2):
                    self.drawPixel(x, y, color)
            for y in range (startY2,stopY2):
                for x in range (width):
                    self.drawPixel(x+xstart, y, color)
        
        pstart = ystart >> 2
        pend= (ystart+height-1) >> 2            
        if (self.c_refresh_start > xstart):
            self.c_refresh_start = xstart
        if (self.c_refresh_end < (xstart+width-1)):
            self.c_refresh_end = (xstart+width-1)
        if (self.p_refresh_start > pstart):
            self.p_refresh_start = pstart
        if (self.p_refresh_end < pend):
            self.p_refresh_end = pend
        self.needRefresh = True
     
    def copyBitmap (self, bitmap, xstart, ystart, width, height):
        xstart, ystart, width, height = self.checkBondaries(xstart,ystart,width,height)
        if (width == 0) or (height == 0):
            return 
        for y in range (height):
                for x in range (width):
                    #self.drawPixel(x+xstart, y+ystart, bitmap[x+(width*y)])
                    self.ram [(x+xstart) + (DOGXL_WIDTH * (y+ystart))] = bitmap[x+(width*y)]
        pstart = ystart >> 2
        pend= (ystart+height-1) >> 2 
        if (self.c_refresh_start > xstart):
            self.c_refresh_start = xstart
        if (self.c_refresh_end < (xstart+width-1)):
            self.c_refresh_end = (xstart+width-1)
        if (self.p_refresh_start > pstart):
            self.p_refresh_start = pstart
        if (self.p_refresh_end < pend):
            self.p_refresh_end = pend
        self.needRefresh = True

    def getColValue (self, c, p):
        y = p << 2
        colValue = self.ram [c + (DOGXL_WIDTH * y)] \
                + (self.ram[c + (DOGXL_WIDTH * (y + 1))] << 2) \
                + (self.ram[c + (DOGXL_WIDTH * (y + 2))] << 4) \
                + (self.ram[c + (DOGXL_WIDTH * (y + 3))] << 6)

        return colValue

    def getRefreshArea (self):
        i = 0
        ret = refreshArea()
        if (self.needRefresh):
            ret.pStart = self.p_refresh_start
            ret.pEnd = self.p_refresh_end
            ret.cStart = self.c_refresh_start
            ret.cEnd = self.c_refresh_end
            ret.area = [0] * (ret.cEnd - ret.cStart + 1) * (ret.pEnd - ret.pStart + 1)
            for p in range (ret.pStart,ret.pEnd+1):
                for c in range (ret.cStart, ret.cEnd+1):
                    ret.area[i] = self.getColValue(c,p)
                    i = i + 1
            self.c_refresh_start = DOGXL_WIDTH
            self.c_refresh_end = -1
            self.p_refresh_start = DOGXL_NUM_PAGES
            self.p_refresh_end = -1
            self.needRefresh = False

            
        
        return ret


class DOGXL():
    "Class to handle communication with EA DOGLXL-160x104 and some basic operations"

    def __init__( self, defaultFont=None):
        self.ram = DogXLRam()
        self.lcdInit()
        self.lcdCls()
        self.defaultFont=defaultFont
  
    def lcdWriteSpi (self, d):      
        for i in range (0, len(d), 4096):
            self.spidev._spi.transfer(d[i:i+4096])


    def lcdCommand( self, d ):
        "Send a command byte or array to the LCD"
        self.cmdPin.off()
        self.lcdWriteSpi(d)
        #time.sleep(0.001)

    def lcdData( self, d ):
        "Send a data byte to the LCD"
        self.cmdPin.on()
        self.lcdWriteSpi(d)
        #time.sleep(0.001)

#  def lcdDataSeq( self, t ):
#        "Send a data sequence to the LCD stored in a list, tuple or string"
#        for i in t:
#            self.lcdData(i)

#    def lcdCommandSeq( self, t ):
#        "Send a command sequence to the LCD stored in a list, tuple or string"
#        for i in t:
#            self.lcdCommand(i)
        

    def lcdInit( self ):
        "GPIO and LCD initialization"
        # Initialize SPI
        self.spidev = SPIDevice(port=0, device=0)
        self.spidev._spi._set_clock_mode(0b11)
        self.spidev._spi._interface.max_speed_hz = 8000000
        self.spidev._spi._set_bits_per_word (8)


        # Initialize GPIO
        self.cmdPin = OutputDevice(DOG_CMD)
        self.rstPin = OutputDevice(DOG_RES)

        # Reset display
        self.rstPin.off()
        time.sleep(0.01)
        self.rstPin.on()
        time.sleep(0.01)
        reset_cmd = [DOGXL_SYSTEM_RESET]
        self.lcdCommand(reset_cmd)
        time.sleep(0.01)

        # Send init sequence
        init_seq = [ DOGXL_SET_COM_END, DOGXL_HEIGHT-1, \
                    DOGXL_SET_PANEL_LOADING, \
                    DOGXL_SET_LCD_BIAS_RATIO, \
                    DOGXL_SET_VBIAS_POT, int(DOGXL_INITIAL_CONTRAST * 254 / 100), \
                    DOGXL_SET_MAPPING_CONTROL, \
                    (DOGXL_SET_SCROLL_LINE_LSB | 0), \
                    (DOGXL_SET_SCROLL_LINE_MSB | 0), \
                    (DOGXL_SET_AC | DOGXL_AC_WA_MASK), \
                    (DOGXL_SET_DISPLAY_ENABLE | 1)]
        self.lcdCommand(init_seq)
    
    def lcdFlush (self):
        "Update display from local content of RAM"
        if (self.ram.needRefresh):
            refresh = self.ram.getRefreshArea()
            cmd = [(DOGXL_SET_WINDOW_PROGRAM_ENABLE | 0), \
                    DOGXL_SET_WP_STARTING_CA, refresh.cStart, \
                    DOGXL_SET_WP_ENDING_CA, refresh.cEnd, \
                    DOGXL_SET_WP_STARTING_PA, refresh.pStart, \
                    DOGXL_SET_WP_ENDING_PA, refresh.pEnd, \
                    (DOGXL_SET_WINDOW_PROGRAM_ENABLE | 1)]
            self.lcdCommand (cmd)
            self.lcdData(refresh.area)

         

    def lcdCls( self, flush=True ):
        "Clear the entire screen"
        self.ram.clearScreen()
        if (flush):
            self.lcdFlush()


    def ldcRect( self, xstart, ystart, width, height, thickness=1, fill=False, color=DOGXL_PIXEL_BLACK, flush=True):
        "Paint a 1px wide rectangle at the edges of the screen, clear the rest."
        self.ram.drawRectangle (xstart, ystart, width, height, thickness, fill, color)
        if (flush):
            self.lcdFlush()

   
    def printText( self, x, y, t, font=None, flush=True, invDir = False ):
        "Prints a text at position x y using the normal font"
        if font==None:
            font=self.defaultFont
        if (invDir):
            for c in reversed(t):
                b = ord(c)
                if b in font:
                    self.ram.copyBitmap (font[b][2], x - font[b][0], y, font[b][0], font[b][1])
                    x = x - font[b][0]
        else:
            for c in t:
                b = ord(c)
                if b in font:
                    self.ram.copyBitmap (font[b][2], x, y, font[b][0], font[b][1])
                    x = x + font[b][0]
        if (flush):
            self.lcdFlush()