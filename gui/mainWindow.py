from gui import display
from gui import rotary
from gpiozero import Button


class mw():
    def __init__(self):
        self._display=display.display()
        self._rotary=rotary.rotary()
        self._pushOk = Button(pin=4, pull_up=True)
        self._pushCancel = Button(pin=14, pull_up=True)
        self._pushMode = Button(pin=15, pull_up=True)
        self._pushOnOff = Button(pin=17, pull_up=True)