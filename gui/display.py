from gui import dogxl
from gui.fonts import *
from gui import waterdrop
from gui import sun
from gui import rain
import time

class display():
    def __init__ (self):
        self._screenWidth = 160
        self._screenHeight = 104

        # Room display position and font.
        self._roomX = 0
        self._roomY = 0
        self._roomFont = Ubuntu12.Ubuntu12
        self._room = ""

        # Mode display position and font.
        self._modeX = 0
        self._modeY = 13
        self._modeFont = Ubuntu20.Ubuntu20
        self._mode = ""

        # Consigne display position and font.
        self._consX = 0
        self._consY = 32
        self._consFont = Ubuntu16.Ubuntu16
        self._cons = ""

        # In temperature display position and font.
        self._inTempX = 159
        self._inTempY = 0
        self._inTempFont = Ubuntu36.Ubuntu36
        self._inTemp = ""

        # Out temperature display position and font.
        self._outTempX = self._inTempX
        self._outTempY = self._inTempY + int(self._screenHeight / 2)
        self._outTempFont = self._inTempFont
        self._outTemp = ""

        # Ext label position and font.
        self._extX = self._roomX
        self._extY = self._roomY + int(self._screenHeight / 2) - 2
        self._extFont = Ubuntu12.Ubuntu12

        # Out humidity display position and font.
        self._outHumX = self._modeX + 12
        self._outHumY = self._extY + 13
        self._outHumFont = Ubuntu20.Ubuntu20
        self._outHum = ""

        self._waterdropX = self._modeX
        self._waterdropY = self._extY + 12

        # Set label position and font.
        self._setX = 0
        self._setY = 72
        self._setFont = Ubuntu28.Ubuntu28

        # Set room label position and font.
        self._setRoomX = 0
        self._setRoomY = 0
        self._setRoomFont = Ubuntu20.Ubuntu20

        # Set mode label position and font.
        self._setModeX = 0
        self._setModeY = 22
        self._setModeFont = Ubuntu20.Ubuntu20
        self._setMode = ""

        # Set mode label position and font.
        self._setTempX = 159
        self._setTempY = 58
        self._setTempFont = Ubuntu44.Ubuntu44
        self._setTemp = ""

        # Meteo babel position and font
        self._meteoX = self._modeX
        self._meteoY = 83
        self._meteoFont = Ubuntu12.Ubuntu12
        self._meteo = ""
        
        # Create Display
        self._disp = dogxl.DOGXL(defaultFont=Ubuntu16.Ubuntu16)

        #draw background
        self.drawMainDispBackground()

    def setTempIn(self, temp):
        if (temp > -99):
            str = "%.1f°" % temp
        else:
            str = "---"
        #starttime = time.perf_counter()
        self.clearLabel(self._inTempX, self._inTempY, self._inTemp , self._inTempFont, True)
        self._disp.printText(self._inTempX, self._inTempY, t=str, font=self._inTempFont, flush=True, invDir=True)
        #rendertime = time.perf_counter() - starttime
        #starttime = time.perf_counter()
        #self._disp.lcdFlush()
        #flushtime = time.perf_counter() - starttime
        #print ("Render time = %fs / flush time = %fs" % (rendertime, flushtime))
        self._inTemp = str


    def setTempOut(self, temp):
        if (temp > -99):
            str = "%.1f°" % temp
        else:
            str = "---"
        self.clearLabel(self._outTempX, self._outTempY, self._outTemp , self._outTempFont, True)
        self._disp.printText(self._outTempX, self._outTempY, t=str, font=self._outTempFont, invDir=True)
        self._outTemp = str

    def setRoom(self, room):
        self.clearLabel(self._roomX, self._roomY, self._room , self._roomFont)
        self._disp.printText(self._roomX, self._roomY, t=room, font=self._roomFont)
        self._room = room

    def setMode(self, mode):
        self.clearLabel(self._modeX, self._modeY, self._mode, self._modeFont)
        self._disp.printText(self._modeX, self._modeY, t=mode, font=self._modeFont)
        self._mode = mode

    def setConsigne(self, consigne):
        str = "%.1f°" % consigne
        self.clearLabel(self._consX, self._consY, self._cons, self._consFont)
        self._disp.printText(self._consX, self._consY, t=str, font=self._consFont)
        self._cons = str

    def clearConsigne(self):
        self.clearLabel(self._consX, self._consY, self._cons, self._consFont)
        self._disp.lcdFlush()
        self._cons = ""
    
    def setHumidity(self, hum):
        if (hum >= 0):
            str = "%d%%" % hum
        else:
            str = "--%"
        self.clearLabel(self._outHumX, self._outHumY, self._outHum, self._outHumFont)
        self._disp.printText(self._outHumX, self._outHumY, t=str, font=self._outHumFont)
        self._outHum = str
    
    def setSetRoom(self, room):
        self._disp.printText(self._setRoomX, self._setRoomY, t=room, font=self._setRoomFont)
    
    def setMeteo (self, dispRain, meteo):
        self.clearLabel(self._meteoX + rain.rain["header"][1] + 4, self._meteoY+5, self._meteo , self._meteoFont)
        if dispRain:
            self._disp.ram.copyBitmap(rain.rain["data"],self._meteoX, self._meteoY, rain.rain["header"][1], rain.rain["header"][2])   
        else:
            self._disp.ram.copyBitmap(sun.sun["data"],self._meteoX, self._meteoY, sun.sun["header"][1], sun.sun["header"][2])

        self._disp.printText (self._meteoX + rain.rain["header"][1] + 4, self._meteoY+5, t=meteo, font=self._meteoFont)
        self._meteo = meteo

    def setSetMode(self, mode):
        self.clearLabel(self._setModeX, self._setModeY, self._setMode, self._setModeFont)
        self._disp.printText(self._setModeX, self._setModeY, t=mode, font=self._setModeFont)
        self._setMode = mode

    def setSetTemp(self, temp):
        str = "%.1f°" % temp
        self.clearLabel(self._setTempX, self._setTempY, self._setTemp , self._setTempFont, True)
        self._disp.printText(self._setTempX, self._setTempY, t=str, font=self._setTempFont, invDir=True)
        self._setTemp = str


    def drawMainDispBackground(self):
        self._disp.lcdCls()
        self._disp.ldcRect(0,int(self._screenHeight/2)-4, self._screenWidth, 2, fill=True, flush=False)
        self._disp.printText(self._extX, self._extY, t="Ext", font=self._extFont)
        self._disp.ram.copyBitmap(waterdrop.waterdrop["data"],self._waterdropX, self._waterdropY, waterdrop.waterdrop["header"][1], waterdrop.waterdrop["header"][2])
        

    def drawSetDispBackground(self):
        self._disp.lcdCls()
        self._disp.printText(self._setX, self._setY, t="Set", font=self._setFont)

    def getLabelSize (self, label, font):
        height = font["data"][2]
        width = 0
        if (isinstance (label, str)):
            for c in label:
                b = ord(c)
                if b in font:
                    width = width + font[b][0]
        return (width, height)

    def clearLabel (self, x, y, label, font, invDir = False):
        width, height = self.getLabelSize (label, font)
        if not invDir:
            self._disp.ldcRect(x,y, width, height, fill=True, flush=False, color=dogxl.DOGXL_PIXEL_WHITE)
        else:
            self._disp.ldcRect(x-width,y, width, height, fill=True, flush=False, color=dogxl.DOGXL_PIXEL_WHITE)
                    


